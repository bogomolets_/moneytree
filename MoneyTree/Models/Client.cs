﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoneyTree.Models
{
    public class Client
    {
        public Client()
        {
            ClientId = ++stID; 
        }
        static public int stID = 0;
        public int ClientId { get; set; }
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Введите имя")]
        public string FName { get; set; }
        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Введите фамилию")]
        public string LName { get; set; }
        [Display(Name = "Отчество")]
        public string MName { get; set; }
        [Display(Name = "ИНН")]
        [Required(ErrorMessage = "Введите ИНН")]
        public string INN { get; set; }
        static public List<Account> Accounts { get; set; } = new List<Account>();
    }
}