﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoneyTree.Models
{
    public class Account
    {
        public Account()
        {
            AccountId = ++stId;
            OpeningDate = DateTime.Now;
        }
        static public int stId = 0;
        public int AccountId { get; set; }
        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Введите номер телефона")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Номер должен содержать 10 цифр")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Номер cчета")]
        public string AccountNumber { get; set; }
        [Display(Name = "Баланс")]
        public decimal Balance { get; set; } = 0;
        [Display(Name = "Дата открытия cчета")]
        public DateTime OpeningDate { get; set; }
        public List<Transaction> transactions { get; set; } = new List<Transaction>();
    }
}