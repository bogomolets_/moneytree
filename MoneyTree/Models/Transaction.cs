﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoneyTree.Models
{
    public class Transaction
    {
        public Transaction()
        {
            TransactionId = ++stId;
        }
        static public int stId = 0;
        public int TransactionId { get; set; }
        [Display(Name = "Номер счета")]
        public string DirectNumber { get; set; }
        [Display(Name = "Наименование счета")]
        public string DirectName { get; set; }
        [Display(Name = "Сумма")]
        public decimal Amount { get; set; }
        [Display(Name = "Номер счета")]
        public decimal CurrentBalance { get; set; }
    }
}